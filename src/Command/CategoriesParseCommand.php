<?php

namespace App\Command;

use App\Service\Category;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class CategoriesParseCommand extends Command
{
    /**
     * @var Category
     */
    private $categoryService;

    public function __construct(Category $categoryService)
    {
        $this->categoryService = $categoryService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:parse_categories')

            // the short description shown while running "php bin/console list"
            ->setDescription('Parse categories.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command parse categories from site roznica.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mainCategories = $this->categoryService->getCategoryRepository()->findBy(['parentId' => 0]);

        foreach ($mainCategories as $category) {
            $this->categoryService->parseCategories($category->getParselink());
        }
    }

}

