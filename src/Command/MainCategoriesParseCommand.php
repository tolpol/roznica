<?php

namespace App\Command;

use App\Service\Category;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MainCategoriesParseCommand extends Command
{
    /**
     * @var Category
     */
    private $categoryService;

    public function __construct(Category $categoryService)
    {
        $this->categoryService = $categoryService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:parse_main_categories')

            // the short description shown while running "php bin/console list"
            ->setDescription('Parse main categories.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command parse main categories from site roznica.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->categoryService->parseMainCategories();
    }

}
