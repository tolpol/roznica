<?php

namespace App\Command;

use App\Service\Product;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class ProductParseCommand extends Command
{
    /**
     * @var Product
     */
    private $productService;

    public function __construct(Product $productService)
    {
        $this->productService = $productService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:parse_product')

            // the short description shown while running "php bin/console list"
            ->setDescription('Parse product.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command parse product from site roznica.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $availableCategories = $this->productService->getCategoryRepository()->getAvailableCategories();
        foreach ($availableCategories as $category) {
            $this->productService->parseAndSaveProducts($category['link'], $category['id']);
        }
    }

}


