<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Service\Category;
use App\Service\Product;
use App\Service\ProductHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin")
 */
class AdminController extends AbstractController
{
    /**
     * @var ProductHelper
     */
    private $productHelper;
    /**
     * @var Product
     */
    private $productService;
    /**
     * @var Category
     */
    private $categoryService;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(
        ProductHelper $productHelper,
        Product $productService,
        Category $categoryService,
        CategoryRepository $categoryRepository
    )
    {
        $this->productHelper = $productHelper;
        $this->productService = $productService;
        $this->categoryService = $categoryService;
        $this->categoryRepository = $categoryRepository;
    }
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $admin = 'admin';

        return $this->render('admin/index.html.twig', ['admin' => $admin]);
    }

    /**
     * @Route("/parse/products/{bool}", name="parse_cats")
     */
    public function parseProducts($bool)
    {
        if ($bool == 1) {
            $availableCategories = $this->categoryRepository->getAvailableCategories();
            foreach ($availableCategories as $category) {
                $this->productService->parseAndSaveProducts($category['link'], $category['id']);
            }
        }

        $admin = 'admin';
        return $this->render('admin/index.html.twig', ['admin' => $admin]);
    }
}