<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

class CategoryRepository extends ServiceEntityRepository
{

    public function __construct(
        ManagerRegistry $registry
    )
    {
        parent::__construct($registry, Category::class);
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function insertCategory($params)
    {
        $stmt = $this->getEntityManager()->getConnection()->prepare("INSERT INTO category
            (title, link, alias, parent_id) VALUES (:title, :link, :alias, :parent_id)
          ");

        $stmt->execute([
            "link" => $params['link'],
            "title" => $params['title'],
            "alias" => $params['alias'],
            "parent_id" => $params['parent_id'],
        ]);
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getAvailableCategories()
    {
        $stmt = $this->getEntityManager()->getConnection()->prepare("SELECT id, link FROM category
            WHERE id NOT IN (SELECT concat(parent_id) FROM category)");
        $stmt->execute();

        $rows = $stmt->fetchAll();

        return $rows;
    }
}

