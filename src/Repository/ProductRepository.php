<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

class ProductRepository extends ServiceEntityRepository
{

    public function __construct(
        ManagerRegistry $registry
    )
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function insertProduct($params)
    {
        $stmt = $this->getEntityManager()->getConnection()->prepare("INSERT INTO product
            (title, link, alias, price, image, category_id) VALUES (:title, :link, :alias, :price, :image, :category_id)
          ");

        $stmt->execute([
            "title" => $params['title'],
            "link" => $params['link'],
            "alias" => $params['alias'],
            "price" => $params['price'],
            "image" => $params['image'],
            "category_id" => $params['category_id'],
        ]);
    }
}