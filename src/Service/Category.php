<?php

namespace App\Service;

use App\Repository\CategoryRepository;

class Category
{
    const SITE = 'http://www.roznica.com.ua/';

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(
        CategoryRepository $categoryRepository
    )
    {
       $this->categoryRepository = $categoryRepository;
    }

    /**
     * @return CategoryRepository
     */
    public function getCategoryRepository(): CategoryRepository
    {
        return $this->categoryRepository;
    }

    public function parseMainCategories()
    {
        $mainPage = file_get_contents( self::SITE);

        $itemsArr = explode('c-nav', $mainPage);

        $items = [];
        $itemsCount = count($itemsArr);
        for ($i = 1; $i < $itemsCount; $i++) {
            $link = explode('href="', $itemsArr[$i]);

            $linkTitleArr = explode('</a>', $link[1]);
            $linkTitle = $linkTitleArr[0];
            $linkTitleArr = explode('" class="mainaimlink">', $linkTitle);

            $categoryLink = $linkTitleArr[0];

            $lastChar = strrpos($categoryLink, '-');
            $categoryAlias = substr($categoryLink, 0, $lastChar);

            $categoryTitle = $linkTitleArr[1];

            $items[] = [
                'link' => $categoryLink,
                'title' => $categoryTitle,
                'alias' => $categoryAlias,
                'parent_id' => 0,
            ];
        }
        unset($items[0]);

        foreach ($items as $item) {
            $this->categoryRepository->insertCategory($item);
        }

        return true;
    }

    public function parseCategories($link)
    {
        $parentId = $this->categoryRepository->findOneBy(['parselink' => $link])->getId();
        $page = file_get_contents(self::SITE . $link);

        $itemsArr = explode('catalog-filter-list', $page);
        if (count($itemsArr) == 2) {
            $itemsArr = explode('/ul>', $itemsArr[1]);

            $items = [];

            $partLink = explode('href="', $itemsArr[0]);
            unset($partLink[0]);

            foreach ($partLink as $part) {
                $titleArr = explode('</a>', $part);
                $linkTitleArr = explode('">', $titleArr[0]);

                $categoryLink = substr($linkTitleArr[0], 1);
                $title = $linkTitleArr[1];

                $lastChar = strrpos($categoryLink, '-');
                $alias = substr($categoryLink, 0, $lastChar);

                $linkArr = explode('"', $part);
                $parts = explode('/', $linkArr[0]);
                if (count($parts) > 2) {
                    continue;
                }

                $item = [
                    'title' => $title,
                    'alias' => $alias,
                    'link' => $categoryLink,
                    'parent_id' => $parentId
                ];

                $this->categoryRepository->insertCategory($item);

                $this->parseCategories($categoryLink);
            }
        }
        return true;
    }
}