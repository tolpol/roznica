<?php

namespace App\Service;

use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\HttpKernel\Log\Logger;

class Product
{
    const SITE = 'http://www.roznica.com.ua/';

    private $categories = [];

    /**
     * @var KernelInterface
     */
    private $appKernel;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * Product constructor.
     * @param ProductRepository $productRepository
     * @param CategoryRepository $categoryRepository
     * @param KernelInterface $appKernel
     */
    public function __construct(
        ProductRepository $productRepository,
        KernelInterface $appKernel,
        CategoryRepository $categoryRepository
    )
    {
       $this->productRepository = $productRepository;
       $this->appKernel = $appKernel;
       $this->categoryRepository = $categoryRepository;
    }
    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }
    /**
     * @return CategoryRepository
     */
    public function getCategoryRepository(): CategoryRepository
    {
        return $this->categoryRepository;
    }

    public function parseAndSaveProducts($categoryLink, $categoryId)
    {
        $i = 0;
        $paginate = true;
        do {
            $pageNumber = $i == 0 ? '' : '?page=' . $i;
            $page = file_get_contents( self::SITE . $categoryLink . $pageNumber);

            if(strpos($page, 'paginator') == false) $paginate = false;

            if ($paginate) {
                $pageArr = explode('row items-container', $page);
                $itemsArr = explode('col-xs-12 col-md-4 col-lg-3 item', $pageArr[1]);

                $itemsCount = count($itemsArr);
                for ($j = 1; $j < $itemsCount; $j++) {
                    $link = explode('href="/', $itemsArr[$j]);

                    if (!isset($link[2])) continue;
                    $titleArr = explode('</a></h6>', $link[2]);
                    if (!isset($titleArr[0])) continue;
                    $productTitle = trim(explode('>', $titleArr[0])[1]);

                    if (!isset($link[1])) continue;
                    $productLinkArr = explode('">', $link[1]);
                    $productLink = $productLinkArr[0];

                    $priceArr = explode('actual-price">', $link[2]);
                    if (!isset($priceArr[1])) continue;
                    $productPrice = explode(' <',  $priceArr[1])[0];

                    $lastChar = strrpos($productLink, '-');
                    $productAlias = substr($productLink, 0, $lastChar);


                    $imageArr = explode('<img src="/', $link[1]);
                    if (!isset($imageArr[1])) continue;
                    $productImage = explode('"', $imageArr[1])[0];

                    $imageUrl = self::SITE . $productImage;
                    $img = file_get_contents($imageUrl);
                    $imagePathArr = explode('/', $productImage);
                    $imageName = end($imagePathArr);
                    $tmp_name = $this->appKernel->getProjectDir() . "\public\images\\" . $imageName;
                    file_put_contents($tmp_name, $img);

                    $item = [
                        'title' => $productTitle,
                        'link' => $productLink,
                        'alias' => $productAlias,
                        'image' => $productImage,
                        'price' => $productPrice,
                        'category_id' => $categoryId
                    ];

                    $this->productRepository->insertProduct($item);
                }

                $i++;
            }
        } while (true);
    }
}