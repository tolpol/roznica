<?php

namespace App\Service;

use App\Repository\ProductRepository;

class ProductHelper
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * ProductHelper constructor.
     * @param ProductRepository $productRepository
     */
    public function __construct(
        ProductRepository $productRepository
    )
    {
       $this->productRepository = $productRepository;
    }


    public function parse()
    {

    }
}